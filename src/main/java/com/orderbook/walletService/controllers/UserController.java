package com.orderbook.walletService.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orderbook.walletService.models.User;
import com.orderbook.walletService.requests.UserRequest;
import com.orderbook.walletService.responsers.UserResponse;
import com.orderbook.walletService.services.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping(value = "/create")
	public ResponseEntity<?> createUser(@RequestBody UserRequest userBody){
		
		User user = userService.createUser(userBody);
		
		return ResponseEntity.ok(new UserResponse(user).toJson());
	}
	
	@GetMapping(value = "/getUser/{idUser}")
	public ResponseEntity<?> getUser(@PathVariable("idUser") Long id) {
		
		User user = userService.findUser(id);
		
		return ResponseEntity.ok(new UserResponse(user).toJson());
		
	}
	

}
