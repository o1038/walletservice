package com.orderbook.walletService.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orderbook.walletService.requests.OrderSendRequest;
import com.orderbook.walletService.requests.TradeRequest;
import com.orderbook.walletService.responsers.OrderResponse;
import com.orderbook.walletService.services.OrderService;
import com.orderbook.walletService.services.WalletService;

@RestController
@RequestMapping(value = "/wallet")
public class WalletController {

	
	@Autowired
	private OrderService saleService;
	
	@Autowired
	private WalletService walletService;
	
	@Autowired
	private Environment env;
	
	private static Logger logger = LoggerFactory.getLogger(WalletController.class);
	
	
	@GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public void list() {

	}
	
	 @PostMapping(value="/sendSale", produces = MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<?> sendOrderSale(@RequestBody OrderSendRequest order){
		 
		 logger.info("========> Wallet Controller PORT: " + env.getProperty("local.server.port"));
		 
		 OrderResponse orderResponse = saleService.createSale(order);
		 
		 return ResponseEntity.ok(orderResponse.toJson());
	 }
	 
	 @PostMapping(value="/sendBuy", produces = MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<?> sendOrderBuy(@RequestBody OrderSendRequest order){
		 
		 OrderResponse orderResponse = saleService.createBuy(order);
		 
		 return ResponseEntity.ok(orderResponse.toJson());
	 }
	 
	 @PostMapping(value="/execute-orders", produces = MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<?> execOrders(@RequestBody TradeRequest request) {
		 
		 walletService.executeTrade(request);
		 
		 return ResponseEntity.ok(null);
	 }
	 
	 

}
