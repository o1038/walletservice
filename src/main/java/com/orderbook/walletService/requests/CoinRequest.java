package com.orderbook.walletService.requests;

public class CoinRequest {

	private Long idCoin;
	private Long amount;
	
	public Long getIdCoin() {
		return idCoin;
	}
	public void setIdCoin(Long idCoin) {
		this.idCoin = idCoin;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	
	
	
}
