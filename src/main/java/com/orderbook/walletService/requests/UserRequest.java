package com.orderbook.walletService.requests;

import java.util.ArrayList;

public class UserRequest {
	
	
	private String name;
	private ArrayList<CoinRequest> coins = new ArrayList<>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<CoinRequest> getCoins() {
		return coins;
	}
	public void setCoins(ArrayList<CoinRequest> coins) {
		this.coins = coins;
	}
	
	
	
	
	

}
