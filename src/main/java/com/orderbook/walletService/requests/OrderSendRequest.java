package com.orderbook.walletService.requests;

public class OrderSendRequest {
	
	private Long idWallet;
	private Long idMarket;
	private Double price;
	private Double amount;
	
	public Long getIdWallet() {
		return idWallet;
	}
	public void setIdWallet(Long idWallet) {
		this.idWallet = idWallet;
	}
	public Long getIdMarket() {
		return idMarket;
	}
	public void setIdMarket(Long idMarket) {
		this.idMarket = idMarket;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	

}
