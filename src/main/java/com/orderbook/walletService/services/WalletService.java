package com.orderbook.walletService.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orderbook.walletService.models.Coin;
import com.orderbook.walletService.models.Wallet;
import com.orderbook.walletService.repository.MoneyService;
import com.orderbook.walletService.repository.WalletRepository;
import com.orderbook.walletService.requests.TradeRequest;

@Service
public class WalletService {
	
	@Autowired
	private WalletRepository walletRepository;
	
	@Autowired
	private MoneyService moneyService;
	
	@Autowired
	private CoinService coinService;
	
	public void executeTrade(TradeRequest request) {

		
		Wallet wallet = walletRepository.getById(request.getIdWallet());
		Coin coinPrincipal = coinService.getCoinById(request.getIdCoinPrincipal());
		Coin coinSecond = coinService.getCoinById(request.getIdCoinSecond());
		Wallet walletTrade = walletRepository.getById(request.getIdWalletTrade());
		
		moneyService.execTrade(wallet, coinPrincipal, request.getAmountPrincipal(), coinSecond, request.getAmountSecond());
		moneyService.execTrade(walletTrade, coinSecond, request.getAmountSecond(),coinPrincipal, request.getAmountPrincipal());
		
	}

}
