package com.orderbook.walletService.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orderbook.walletService.models.Coin;
import com.orderbook.walletService.models.Money;
import com.orderbook.walletService.models.User;
import com.orderbook.walletService.models.Wallet;
import com.orderbook.walletService.repository.MoneyService;
import com.orderbook.walletService.repository.UserRepository;
import com.orderbook.walletService.repository.WalletRepository;
import com.orderbook.walletService.requests.CoinRequest;
import com.orderbook.walletService.requests.UserRequest;

@Service
public class UserService {
	
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private WalletRepository walletRepository;
	
	@Autowired
	private CoinService coinService;
	
	@Autowired
	private MoneyService moneyService;
	
	public User createUser(UserRequest request) {
		
		Wallet wallet = new Wallet();
		
		walletRepository.save(wallet);
		
		ArrayList<Money> moneys = getMoneys(request.getCoins(), wallet);
		
		moneyService.insertMoneys(moneys);
		
		wallet.setMoneys(moneys);
				
		User user = new User(request.getName(), wallet);
				
		return userRepository.save(user);
	}
	
	
	public ArrayList<Money> getMoneys(ArrayList<CoinRequest> coinsRequest, Wallet wallet) {
		
		ArrayList<Money> moneys = new ArrayList<>();
		
		List<Coin> coins = coinService.getCoins();
		
		coins.stream().forEach(c -> {
			moneys.add(new Money(c, 0.0));
		});
		
		coinsRequest.forEach(c -> {
			moneys.forEach(m -> {
				if(c.getIdCoin() == m.getCoin().getId()) {
					m.setAmount(m.getAmount()+c.getAmount());
				}
				m.setWallet(wallet);
			});
		});
		
		return moneys;
		
	}
	
	public User findUser(Long id) {
		
		User user = new User();
		Optional<User> opUser = userRepository.findById(id);
		if(opUser.isPresent()) {
			user = opUser.get();
			List<Money> moneys = moneyService.getByWallet(user.getWallet());
			user.getWallet().setMoneys(moneys);
			
		}
		
		return user;
		
	}

}













