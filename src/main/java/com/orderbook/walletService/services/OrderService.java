package com.orderbook.walletService.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orderbook.walletService.feignclients.OrderBookFeignClient;
import com.orderbook.walletService.rabbitmq.QueueSender;
import com.orderbook.walletService.requests.OrderSendRequest;
import com.orderbook.walletService.responsers.OrderResponse;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@Service
public class OrderService {
	
	@Autowired
	private OrderBookFeignClient orderBookFeignClient;
	
	@Autowired
	private QueueSender queueSender;

    @CircuitBreaker(name = "orderFallBack", fallbackMethod = "sendSaleQueue")
    public OrderResponse createSale(OrderSendRequest order){

        OrderResponse orderResponse = orderBookFeignClient.createSale(order).getBody();
        
        return orderResponse;

    }
    
    @CircuitBreaker(name = "orderFallBack", fallbackMethod = "sendBuyQueue")
    public OrderResponse createBuy(OrderSendRequest order){

        OrderResponse orderResponse = orderBookFeignClient.createBuy(order).getBody();
        
        return orderResponse;

    }
    
    public OrderResponse sendSaleQueue(OrderSendRequest order, Exception e) {
    	
    	System.out.println("Sale in queue");
    	
    	queueSender.send(order, "sale");
    	
    	OrderResponse response = new OrderResponse();
   
         return response;
    	
    }
    
    public OrderResponse sendBuyQueue(OrderSendRequest order, Exception e) {
    	
    	System.out.println("Buy in queue");
    	
    	queueSender.send(order, "buy");
    	
    	OrderResponse response = new OrderResponse();
   
         return response;
    	
    }
    



}
