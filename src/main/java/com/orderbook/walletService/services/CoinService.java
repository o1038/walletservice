package com.orderbook.walletService.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orderbook.walletService.models.Coin;
import com.orderbook.walletService.repository.CoinRepository;

@Service
public class CoinService {
	
	@Autowired
	private CoinRepository coinRepository;
	
	public Coin intertCoin(Coin coin) {
		
		return coinRepository.save(coin);
		
	}
	
	public List<Coin> getCoins(){
		
		return coinRepository.findAll();
		
	}
	
	public Coin getCoinById(Long id) {
		
		return coinRepository.getById(id);
	}

}
