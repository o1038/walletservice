package com.orderbook.walletService.responsers;

import com.orderbook.walletService.requests.OrderRequest;

public class TradeResponse {

	private OrderRequest sale;
	private OrderRequest buy;
		
	public TradeResponse(OrderRequest sale, OrderRequest buy) {
		super();
		this.sale = sale;
		this.buy = buy;
	}
	
	public OrderRequest getSale() {
		return sale;
	}
	public void setSale(OrderRequest sale) {
		this.sale = sale;
	}
	public OrderRequest getBuy() {
		return buy;
	}
	public void setBuy(OrderRequest buy) {
		this.buy = buy;
	}
	
	
	
}
