package com.orderbook.walletService.responsers;

import java.util.HashMap;
import java.util.List;

import com.orderbook.walletService.models.Money;
import com.orderbook.walletService.models.Wallet;

public class WalletResponser extends AbstractResponse{
	
	public WalletResponser(Wallet wallet) {
		this.id = wallet.getId();
		
		List<Money> moneys = wallet.getMoneys();
		for(Money m : moneys) {
			this.moneys.put(m.getCoin().getName(), m.getAmount());
		}
		
	}
	
	
	private Long id;
	private HashMap<String, Double> moneys = new HashMap<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public HashMap<String, Double> getMoneys() {
		return moneys;
	}
	public void setMoneys(HashMap<String, Double> moneys) {
		this.moneys = moneys;
	}
	
	

}
