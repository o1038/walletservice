package com.orderbook.walletService.responsers;

public class OrderResponse extends AbstractResponse{
	
	private Long id;
	private Long idWallet;
	private Long idCoin;
	private Double price;
	private Double amount;
	private String status;
	private Double traded;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdWallet() {
		return idWallet;
	}
	public void setIdWallet(Long idWallet) {
		this.idWallet = idWallet;
	}
	public Long getIdCoin() {
		return idCoin;
	}
	public void setIdCoin(Long idCoin) {
		this.idCoin = idCoin;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getTraded() {
		return traded;
	}
	public void setTraded(Double traded) {
		this.traded = traded;
	}
	
	

}
