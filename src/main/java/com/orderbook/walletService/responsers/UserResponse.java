package com.orderbook.walletService.responsers;

import com.orderbook.walletService.models.User;

import lombok.Data;

@Data
public class UserResponse extends AbstractResponse{
	
	public UserResponse(User user) {
		this.id = user.getId();
		this.name = user.getName();
		this.wallet = new WalletResponser(user.getWallet());
	}
	
	private Long id;
	private String name;
	private WalletResponser wallet;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public WalletResponser getWallet() {
		return wallet;
	}
	public void setWallet(WalletResponser wallet) {
		this.wallet = wallet;
	}
	
	

}
