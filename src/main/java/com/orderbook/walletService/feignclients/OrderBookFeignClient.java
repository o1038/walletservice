package com.orderbook.walletService.feignclients;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.orderbook.walletService.requests.OrderSendRequest;
import com.orderbook.walletService.responsers.OrderResponse;

@Component
@FeignClient(name = "order-book-service")
@LoadBalancerClient(name = "order-book-service")
public interface OrderBookFeignClient {
	
	@PostMapping(value="/sale/create")
	public ResponseEntity<OrderResponse> createSale(@RequestBody OrderSendRequest order);
	
	@PostMapping(value="/buy/create")
	public ResponseEntity<OrderResponse> createBuy(@RequestBody OrderSendRequest order);

}
