package com.orderbook.walletService.rabbitmq;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.orderbook.walletService.requests.OrderSendRequest;
import org.springframework.amqp.core.MessageProperties;

@Component
public class QueueSender {
	
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private Queue queue;

    public void send(String order) {
        rabbitTemplate.convertAndSend(this.queue.getName(), order);
    }
    
    public void send(OrderSendRequest order, String tipo) {
    	
    	String mensagem = new Gson().toJson(order);
    	
    	MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("tipo", tipo);
        Message message = new Message(mensagem.getBytes(), messageProperties);
    	
        rabbitTemplate.convertAndSend(this.queue.getName(), message);
    }

}
