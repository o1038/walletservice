package com.orderbook.walletService.repository;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orderbook.walletService.models.Coin;
import com.orderbook.walletService.models.Money;
import com.orderbook.walletService.models.Wallet;

@Service
public class MoneyService {
	
	@Autowired
	private MoneyRepository moneyRepository;
	
	public List<Money> insertMoneys(List<Money> moneys) {
		
		return moneyRepository.saveAll(moneys);
		
	}
	
	public Money save(Money money) {
		return moneyRepository.save(money);
	}
	
	public List<Money> getByWallet(Wallet wallet) {
		return moneyRepository.findByWallet(wallet);
	}
	
	public void execTrade(Wallet wallet,Coin coinSale, Double amountSale, Coin coinBuy, Double amountBuy) {
		
		List<Money> moneys = getByWallet(wallet);
		
		Double totalSale = 0.0;
		Double totalBuy = 0.0;
		
		for(Money m : moneys) {
			if(m.getCoin().equals(coinSale)) {
				totalSale = m.getAmount() - amountSale;
				m.setAmount(totalSale);
				
			} else  
			if(m.getCoin().equals(coinBuy)) {
				totalBuy = m.getAmount() + amountBuy;
				m.setAmount(totalBuy);
				
			}
		}
		
		moneyRepository.saveAll(moneys);
		
	}
	

}
