package com.orderbook.walletService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orderbook.walletService.models.Coin;

@Repository
public interface CoinRepository extends JpaRepository<Coin, Long>{

	
	
	
}
