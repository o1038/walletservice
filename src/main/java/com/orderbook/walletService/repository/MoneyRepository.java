package com.orderbook.walletService.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orderbook.walletService.models.Money;
import com.orderbook.walletService.models.Wallet;

@Repository
public interface MoneyRepository extends JpaRepository<Money, Long> {
	
	public List<Money> findByWallet(Wallet wallet);

}
