package com.orderbook.walletService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orderbook.walletService.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
