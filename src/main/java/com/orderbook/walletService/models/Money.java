package com.orderbook.walletService.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tb_money")
public class Money {

	public Money(Coin coin, Double amount) {
		super();
		this.coin = coin;
		this.amount = amount;
	}
	
	public Money() {
		super();
	}

	public Money(Long id, Coin coin, Wallet wallet, Double amount) {
		super();
		this.id = id;
		this.coin = coin;
		this.wallet = wallet;
		this.amount = amount;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_coin")
	private Coin coin;

	
	@ManyToOne
	@JoinColumn(name = "id_wallet")
	private Wallet wallet;

	@Column(name = "amount")
	private Double amount;

	public Coin getCoin() {
		return coin;
	}

	public void setCoin(Coin coin) {
		this.coin = coin;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

}
