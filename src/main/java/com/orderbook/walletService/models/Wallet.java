package com.orderbook.walletService.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tb_wallet")
public class Wallet {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@OneToMany(mappedBy="wallet")
	private List<Money> moneys = new ArrayList<Money>();
	public void setId(Long id) {
		this.id = id;
	}

	public List<Money> getMoneys() {
		return moneys;
	}

	public void setMoneys(ArrayList<Money> moneys) {
		this.moneys = moneys;
	}

	public Long getId() {
		return id;
	}

	public void setMoneys(List<Money> moneys) {
		this.moneys = moneys;
	}
	
	
	
	

	
	
	
	
}
